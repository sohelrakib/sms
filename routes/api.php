<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('smsReceive', 'SmsController');

//Route::post('smsReceive', 'SmsController@store');

//Route::resource('smsSent','SmsSentController');
Route::resource('ussd','UssdController');

Route::post('uussd','UussdControl@uussd');

