<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ussd extends Model
{
    protected $table="ussd";

    protected $fillable=[
     
        'raw_receive',
        'raw_request',
        'raw_response',
        'status_code',
        'status_detail'
    ];
}
