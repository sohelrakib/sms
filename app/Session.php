<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $table="session";

    protected $fillable=[
         'session_num'
    ];
}
