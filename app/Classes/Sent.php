<?php
namespace App\Classes;
use App\Classes\Core;
use App\Classes\SMSServiceException;
class Sent extends Core{

  

  	private $applicationId,
  			$password,
  			$charging_amount,
  			$encoding,
  			$version,
  			$deliveryStatusRequest,
  			$binaryHeader,
  			$sourceAddress,
  			$serverURL,

        $raw_response,
        $status_code,
        $status_detail,
        $return_raw_responce,
        $return_jsonStream;
  	
  	/* Send the server name, app password and app id
  	*	Dialog Production Severurl : HTTPS : - https://api.dialog.lk/sms/send
  	*				     HTTP  : - http://api.dialog.lk:8080/sms/send
  	*/	
  	
  	//http://localhost:7000/sms/send 


  	public function __construct($serverURL, $applicationId, $password)
  	{
  		if(!(isset($serverURL, $applicationId, $password)))
  			throw new SMSServiceException('Request Invalid.', 'E1312');
  		else {
  			$this->applicationId = $applicationId;
  			$this->password = $password;
  			$this->serverURL = $serverURL;
  		}
  	}
  	
  	// Broadcast a message to all the subcribed users
  	public function broadcast($message){
  		return $this->sms($message, array('tel:all'));
  	}
  	
  	// Send a message to the user with a address or send the array of addresses
  
 
    public function sms($message, $addresses,$encoded=0){
            $this->encoding=$encoded;
            if(empty($addresses))
                throw new SMSServiceException('Format of the address is invalid.', 'E1325');
            else {
                $jsonStream = (is_string($addresses))?$this->resolveJsonStream($message, array($addresses)):(is_array($addresses)?$this->resolveJsonStream($message, $addresses):null);
                return ($jsonStream!=null)?$this->handleResponse(json_decode($this->sendRequest($jsonStream,$this->serverURL))):false;
               
           
           }
        }
  	
  private function handleResponse($jsonResponse){

  	  $this->return_raw_responce=$jsonResponse; 
  		$statusCode = $jsonResponse->statusCode;
  		$statusDetail = $jsonResponse->statusDetail;
      
      $this->status_code=$statusCode;
      $this->status_detail=$statusDetail;

  		
  		if(empty($jsonResponse))
  			throw new SMSServiceException('Invalid server URL', '500');
  		else if(strcmp($statusCode, 'S1000')==0)
  			return true;
  		else
  			throw new SMSServiceException($statusDetail, $statusCode);
  	}
  	
  	public function resolveJsonStream($message, $addresses){
  		
  		$messageDetails = array("message"=>$message,
  	   	           				"destinationAddresses"=>$addresses
             					);

      $this->raw_response=$messageDetails;
  		
  		if (isset($this->sourceAddress)) {
  			$messageDetails= array_merge($messageDetails,array("sourceAddress" => $this->sourceAddress));   
  		}
  		
  		if (isset($this->deliveryStatusRequest)) {
  			$messageDetails= array_merge($messageDetails,array("deliveryStatusRequest" => $this->deliveryStatusRequest));
  		}
  		
  		if (isset($this->binaryHeader)) {
  			$messageDetails= array_merge($messageDetails,array("binaryHeader" => $this->binaryHeader));
  		}	
  		
  		if (isset($this->version)) {
  			$messageDetails= array_merge($messageDetails,array("version" => $this->version)); 
  		}	
  		
  		if (isset($this->encoding)) {
  			$messageDetails= array_merge($messageDetails,array("encoding" => $this->encoding)); 
  		}
  		
  		$applicationDetails = array('applicationId'=>$this->applicationId,
  						 'password'=>$this->password,);
  		
  		$jsonStream = json_encode($applicationDetails+$messageDetails);
  		
      $this->return_jsonStream=$jsonStream;
  		return $jsonStream;
  	}

  	public function setsourceAddress($sourceAddress){
  		$this->sourceAddress=$sourceAddress;
  	}

  	public function setcharging_amount($charging_amount){
  		$this->charging_amount=$charging_amount;
  	}

  	public function setencoding($encoding){
  		$this->encoding=$encoding;
  	}

  	public function setversion($version){
  		$this->version=$version;
  	}

  	public function setbinaryHeader($binaryHeader){
  		$this->binaryHeader=$binaryHeader;
  	}

  	public function setdeliveryStatusRequest($deliveryStatusRequest){
  		$this->deliveryStatusRequest=$deliveryStatusRequest;
  	}

    public function getRawResponse()
    {
      return $this->raw_response;
    }
  
    public function getStatusCode()
    {
      
      return $this->status_code;
    }

    public function getStatusDetail()
    {
      
      return $this->status_detail;
    }

    public function getRawResponsee()
    {
      return $this->return_raw_responce;
    }

    public function getJsonStream()
    {
      return $this->return_jsonStream;
    }
   
    public function aaaaa()
    {
    	$bb="status:10001";
    	return $bb;
    }

	  
	
}