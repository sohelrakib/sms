<?php
namespace App\Classes;
use App\Classes\Core;
use App\Classes\CassException;

class DirectDebitSender extends core{
    var $server;
    var $applicationId;
    var $password;

    var $status_Code;
    var $status_Detail;
    var $charge_request;
    var $charge_response;
			

    public function __construct($server,$applicationId,$password){
        $this->server = $server;
        $this->applicationId = $applicationId;
        $this->password = $password;
    }

    /*
        Get parameters form the application
        check one or more addresses
        Send them to cassMany
    **/
    public function cass( $externalTrxId, $subscriberId, $amount){
       
        if (is_array($subscriberId)) {
            return $this->cassMany( $externalTrxId, $subscriberId,  $amount);
        } else if (is_string($subscriberId) && trim($subscriberId) != "") {
            return $this->cassMany( $externalTrxId, $subscriberId,  $amount);
        } else {
            throw new Exception("Address should be a string or a array of strings");
        }
    }
	

    private function cassMany($externalTrxId, $subscriberId, $amount){
        $arrayField = array(
				        	"applicationId" => $this->applicationId, 
				            "password" => $this->password,
				            "externalTrxId" => $externalTrxId,
				            "subscriberId" => $subscriberId,
				            "amount" => $amount
				        );
        $jsonObjectFields = json_encode($arrayField); 
        $this->charge_request=$jsonObjectFields;
        return $this->handleResponse(json_decode($this->sendRequest($jsonObjectFields,$this->server)));
    }
	

    private function handleResponse($jsonResponse){
    
        if(empty($jsonResponse))
            throw new CassException('Invalid server URL', '500');
        
        $statusCode = $jsonResponse->statusCode;
        $statusDetail = $jsonResponse->statusDetail;

       $this->status_Code=$statusCode;
       $this->status_Detail=$statusDetail;
       $this->charge_response=$jsonResponse;
        
        if(strcmp($statusCode, 'S1000')==0)
            return 'ok';
        else
            return $statusDetail." , ".$statusCode;
    }

    public function status_code()
    {
    	return $this->status_Code;
    }

    public function status_detail()
    {
    	return $this->status_Detail;
    }

    public function charge_request()
    {
    	return $this->charge_request;
    }

    public function charge_response()
    {
    	return $this->charge_response;
    }

    public  function generate_rand_num()
    {

      $today = date("YmdHis"); 

      $rand1 = '123456';
      $rand1 = str_shuffle($rand1);
      
      $rand2 = '7890';
      $rand2 = str_shuffle($rand2);
      
      $externalTrxId=$rand1.$today.$rand2;
      return  $externalTrxId;

    }

}











