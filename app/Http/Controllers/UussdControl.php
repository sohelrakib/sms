<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\UssdSender;
use App\Classes\DirectDebitSender;
use App\Ussd;
use App\Charge;
use App\Session;

class UussdControl extends Controller
{
    
        private $sourceAddress; 
        private $message;
        private $requestId;
        private $applicationId;
        private $encoding;
        private $version;
        private $sessionId;
        private $ussdOperation;
        private $vlrAddress;
    	  private $thejson;

   public function phone_explode($phone)
   {
   
      $phone=explode(':',$phone);
      $phone=end($phone);
      return $phone;

   }

   public function rand_password_generator()
   { 
         $rand1 = 'abcd';
         $rand1 = str_shuffle($rand1);
         
         $rand2 = '1230';
         $rand2 = str_shuffle($rand2);
         
         $password=$rand1.$rand2;
         return $password;

   }

   public function debit($password)
   {
          $directdebit2=new DirectDebitSender('http://localhost:7000/caas/direct/debit ',$this->applicationId,$password); 
          $externalTrxId=$directdebit2->generate_rand_num(); 
          $check_balance=$directdebit2->cass($externalTrxId,$this->sourceAddress,2.5);


   	     $input3['status_code']=$directdebit2->status_code();
         $input3['status_detail']=$directdebit2->status_detail();
         $input3['raw_request']=$directdebit2->charge_request();
         $input3['raw_response']=json_encode($directdebit2->charge_response());
         Charge::create($input3);
     
         return $check_balance;
   }

   
   public function session_create($session_num)
   {

     $yes_no=$this->session_check($session_num);

     if($yes_no=="0"){
        $input['session_num']=$session_num;
        Session::create($input);
                      }

        return "1";
   }


   public function session_check($session_num)
   {
     
      $data=Session::where('session_num', $session_num)->first();

        if(count($data)==0)
            return "0";
          else
            return "1";
   }

   public function session_delete($session_num)
   {
     
      $data=Session::where('session_num', $session_num)->first();

     if(count($data))
       $data->delete();
      
            return "0";
        
   }




    public function uussd(Request $request)
    {
    	$array = json_decode(file_get_contents('php://input'), true);
    	file_put_contents("test2.txt",print_r($array,true),FILE_APPEND | LOCK_EX);
    	       $this->thejson = json_decode(file_get_contents('php://input'), true);
    	       $this->sourceAddress = $array['sourceAddress'];
    	       $this->message = $array['message'];
    	       $this->requestId = $array['requestId'];
    	       $this->applicationId = $array['applicationId'];
    	       $this->encoding = $array['encoding'];
    	       $this->version = $array['version'];
    	       $this->sessionId = $array['sessionId'];
    	       $this->ussdOperation = $array['ussdOperation'];

         if (!((isset($this->sourceAddress) && isset($this->message)))) {
             throw new Exception("Some of the required parameters are not provided");
         } else {
             $responses = array("statusCode" => "S1000", "statusDetail" => "Success");
         }


         
          $session_condition ='';

         if($array['ussdOperation']=='mo-cont' && $array['message']=='3')
         {
            $session_condition=$this->session_delete($array['sessionId']);
         }
         elseif($array['ussdOperation']=='mo-init')
         {
           $session_condition=$this->session_create($array['sessionId']); 
         } 
         elseif($array['ussdOperation']=='mo-cont')
         {
           $session_condition=$this->session_check($array['sessionId']); 
         }            
         

    
        
         $phone=$this->phone_explode($this->sourceAddress);
         $password=$this->rand_password_generator();

         $ussd_obj=new UssdSender('http://localhost:7000/ussd/send',$this->applicationId,$password);

            
    
       
         if( $array['ussdOperation']!='mo-init' && $session_condition=='0' ){
                   $message="you already have exited. \n Press mo-init to initialize again";
                  
                  }


         
         elseif(( $array['ussdOperation']=='mo-init') || ($array['message']=='0' && $array['ussdOperation']=='mo-cont')){
                   $message="menu: \n 1 : health \n 2 : food \n 3 : exit ";
                  
                  }
                 

        

        // elseif($array['message'] !='*999#' && $array['ussdOperation']=='mo-init')
        //            $message="<h4> Press *999# to go to the menu </h4>";



         elseif($array['message']=='3' && $array['ussdOperation']=='mo-cont'){
                    $message=" exiting ... ";
                   
                  }




         elseif($array['message']=='2' && $array['ussdOperation']=='mo-cont'){
              
                     $status=$this->debit($password);

                     if($status=='ok')
                     	  $message="FOOD TIPS: Try to avoid junk food. \n press 0 to go back to the menu, 3 for exit";
                     else 
                          $message=	"FOOD TIPS: ".$status."\n press 0 to go back to the menu, 3 for exit";
         }

          elseif($array['message']=='1' && $array['ussdOperation']=='mo-cont'){
              
                     $status=$this->debit($password);

                     if($status=='ok')
                        $message="HEALTH TIPS: Walk at list one hour. \n press 0 to go back to the menu, 3 for exit";
                     else 
                          $message= "HEALTH TIPS: ".$status."\n press 0 to go back to the menu, 3 for exit";
         }



         // elseif($array['message']=='1' && $array['ussdOperation']=='mo-cont') {
          
               
         //             $status=$this->debit($password);

         //             if($status=='ok')
         //                $message="HEALTH TIPS: Walk at list one hour in a day. \n press 0 to go back to the menu, 3 for exit";
         //             else 
         //                  $message= "HEALTH TIPS: ".$status."\n press 0 to go back to the menu, 3 for exit";
         // }


        
         else
                $message="wrong key \n press 0 to go back to the menu, 3 for exit";	






         // $input3['status_code']=$directdebit2->status_code();
         // $input3['status_detail']=$directdebit2->status_detail();
         // $input3['raw_request']=$directdebit2->charge_request();
         // $input3['raw_response']=json_encode($directdebit2->charge_response());
         // Charge::create($input3);



         $ussd_obj->ussd($this->sessionId,$message,$phone,$this->ussdOperation);

          

         $input['raw_receive']=json_encode($array);
         $input['raw_request']=$ussd_obj->raw_request();
         $input['raw_response']=$ussd_obj->raw_response();
         $input['status_code']= $ussd_obj->status_code();
         $input['status_detail']= $ussd_obj->status_detail();
         Ussd::create($input);

    }






   
 }
