<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sms_table;
use App\Subscribe;
use App\Charge;
use App\Classes\Sent;
use App\Classes\DirectDebitSender;

class SmsController extends Controller
{
    private $version;
    private $applicationId;     
    private $sourceAddress; 
    private $message;           
    private $requestId;         
    private $encoding;          
    private $thejson;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function index()
    {
       
        // $word = "Many Blocks";
        // if ( preg_match("~\bBlocks\b~",$word) )
        //   echo "matched";
        // else
        //   echo "not match";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    public function aaa()
    {
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   

  

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    

    
    public function store(Request $request)
         {
            $sub=0;
            $unsub=0;
            $notSubOrUnsub=0;
            $balance=1;
            $sub_message='';

               $jsonRequest = json_decode(file_get_contents('php://input'));
               file_put_contents("test.txt",print_r($jsonRequest,true),FILE_APPEND | LOCK_EX);

             
                       
                       if(strlen($jsonRequest->sourceAddress)<8 || $jsonRequest->message=='')
                        {
                           $response = array('statusCode'=>'E1312', 'statusDetail'=>'Request is Invalid.');
                        }

                    else{
                           $this->thejson=$jsonRequest ;
                           $this->version = $jsonRequest->version;
                           $this->applicationId = $jsonRequest->applicationId;
                           $this->sourceAddress = $jsonRequest->sourceAddress;
                           $this->message = $jsonRequest->message;
                           $this->requestId = $jsonRequest->requestId;
                           $this->encoding = $jsonRequest->encoding;
                       
                           $response = array('statusCode'=>'S1000',
                                     'statusDetail'=>'Process completed successfully.');
                        }

               header('Content-type: application/json');
               echo json_encode($response);

              $word = "";
                 

              if(strlen($jsonRequest->sourceAddress)>8 && $jsonRequest->message!='')
                      {    
                            
                            
                            if ( preg_match("~\bsub\b~",$this->message) ){
                                     $word="sub";
                                                                         }
                            elseif ( preg_match("~\bunsub\b~",$this->message) ){
                                     $word="unsub";
                                                                         }                                             
                             


                          if($word=="sub")
                          {



                     $output=Subscribe::where('msisdn',$jsonRequest->sourceAddress)->first();
                     
                                if(count($output)==0 || $output['status']==0){
              $directdebit=new DirectDebitSender('http://localhost:7000/caas/direct/debit ',$this->applicationId,'12345'); 
                $externalTrxId=$directdebit->generate_rand_num(); 
              $check=$directdebit->cass($externalTrxId,$this->sourceAddress,5);

                                         if($check=='ok'){     

                                             $input2['msisdn']=$jsonRequest->sourceAddress;
                                             $input2['status']=1;
                            
                                             Subscribe::create($input2);
                                         }else{
                                              $balance=0;
                                             $sub_message="you can not subscribe because of ".$check;
                                         } 
                                $input3['status_code']=$directdebit->status_code();
                                $input3['status_detail']=$directdebit->status_detail();
                                $input3['raw_request']=$directdebit->charge_request();
                                $input3['raw_response']=json_encode($directdebit->charge_response());
                                Charge::create($input3);

                                }else{
                                    $sub=1;
                                }


                          }elseif($word=="unsub")
                           {
                      $output=Subscribe::where('msisdn',$jsonRequest->sourceAddress)->first();
                             
                                   if(count($output)==0){
                                       $unsub=2;
                                    } elseif(count($output)==1 && $output['status']==1){
                                       // $input2['msisdn']=$jsonRequest->sourceAddress;
                                       $input2['status']=0;
                             
                                       $output->update($input2);
                                       //dd($output);
                                     }else{
                                        $unsub=1;
                                      }

                            }else{
                               $notSubOrUnsub=1;
                           }

              
                      }

              $sent_message='';

              if($balance==0)
                      $sent_message=$sub_message;
              elseif($word=='sub' && $sub==0)
                      $sent_message='successfully subscribed';
              elseif($word=='sub' && $sub==1)
                      $sent_message='you already have subscribed';
              elseif($word=='unsub' && $unsub==2)
                      $sent_message='you are not a subscriber to unsubscribe it!';
              elseif($word=='unsub' && $unsub==0)
                      $sent_message='successfully un-subscribed'; 
              elseif($word=='unsub' && $unsub==1)
                      $sent_message='you already have unsubscribed';
              elseif($notSubOrUnsub==1)
                      $sent_message='you press wrong key word'; 
            
                
              $phone=explode(':', $this->sourceAddress);
              $phone= end($phone);
              $jsondata=json_encode($jsonRequest);
                
            $sent=new Sent('http://localhost:7000/sms/send',$this->applicationId,'12345');
            

            
              
            //$raw_responce=$sent->sendRequest($jsondata,'http://localhost:7000/sms/send');
            $data=$sent->sms($sent_message, $phone);
            $raw_request=$sent->getJsonStream();
             
           $raw_responce=$sent->getRawResponse();

                $status_detail=$sent->getStatusDetail();
                $status_code=$sent->getStatusCode();
                $raw_responcee=$sent->getRawResponsee();

          

             $inputs['row_receive']= json_encode($jsonRequest);
             $inputs['row_request']= $raw_request;
             $inputs['row_response']=json_encode($raw_responcee);
             $inputs['status_code']=$status_code;
             $inputs['status_detail']=$status_detail; 
             Sms_table::create($inputs);
             
 
             
             // $inputs['status_code']=json_encode($response['statusCode']);
             // $inputs['status_detail']=json_encode($response['statusDetail']);

         }




}














