<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    protected $table="charge";

    protected $fillable=[
         
         'raw_request',
         'raw_response',
         'status_code',
         'status_detail'
    ];
}
