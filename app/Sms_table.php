<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms_table extends Model
{
    protected $table="sms_table";

    protected $fillable=[
       'row_receive',
       'row_request',
       'row_response',
       'status_code',
       'status_detail'
    ];
}
